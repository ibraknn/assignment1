package week3;


import java.util.ArrayList;

public class User extends MyApplication {
    // id (you need to generate this id by static member variable)
    // name, surname
    // username
    // password

    private int id;
    private static int id_gen = 0;
    private String name;
    private String surname;
    private String username;
    private String password;
    private String message;


    private void genId() {
        id = id_gen++;
    }

    public User() {

    }

    public User(String username, String password) {
        setUsername(username);
        setPassword(password);
    }

    public User(String message) {
        setMessage(message);
    }

    public User(String name, String surname, String username, String password) {
        genId();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public User(int id, String name, String surname, String username, String password) {
        id_gen = id + 1;
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setGenId(int id) {
        this.id_gen = id + 1;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        if (Password.checkPasswordFormat(password)) {
            this.password = password;
        }
    }

    public int getGetId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname + " " + username + " " + password;
    }
}
