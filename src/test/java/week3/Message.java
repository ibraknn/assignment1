package week3;


public class Message extends User {
    private int firstid;
    private String message;
    private int secid;


    public Message(int firstid, String message, int secid) {
        setMessage(message);
        setFirstId(firstid);
        setSecid(secid);
    }

    public Message(String name, String surname, String username, String password, int firstid, String message, int secid) {
        super(name, surname, username, password);
        setMessage(message);
        setFirstId(firstid);
        setSecid(secid);

    }

    public void setFirstId(int firstid) {
        this.firstid = firstid;
    }

    public int getFirstid() {
        return firstid;
    }

    public void setSecid(int secid) {
        this.secid = secid;
    }

    public int getSecid() {
        return secid;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return firstid + " " + message + " " + secid ;
    }
}
