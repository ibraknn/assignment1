package week3;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication extends Main {
    // users - a list of users
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private ArrayList<User> users = new ArrayList<>();
    private ArrayList<Message> message1 = new ArrayList<>();
    private ArrayList<Friend> friends = new ArrayList<>();

    

    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                for (int i = 0; i < 10; i++) {
                    System.out.println("\n");
                }
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Back");
                int choice = sc.nextInt();
                if (choice == 1) {
                    authentication();
                    break;
                } else {
                    start();
                    break;
                }
            } else break;
        }
    }

    private void userProfile(User users) throws IOException {
        File file = new File("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3\\message.txt");
        Scanner aa = new Scanner(file);
        File fil = new File("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3\\friend.txt");
        Scanner bb = new Scanner(fil);
        System.out.println("userProfile:");
        System.out.println("Id: " + users.getGetId());
        System.out.println("Name: " + users.getName());
        System.out.println("Surname: " + users.getSurname());
        System.out.println("Username: " + users.getUsername());
        System.out.println("Password: " + users.getPassword());
        System.out.println("----------------");
        while (true) {
            System.out.println("1. Other users and message box");
            System.out.println("2. Friend list");
            System.out.println("3. Log Off");
            System.out.println("4. Close application");
            int choice = sc.nextInt();
            int idd;
            String messages;
            if (choice == 1) {
                File files = new File("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3\\data.txt");
                Scanner scan = new Scanner(files);

                int counter = 0;

                while (scan.hasNext()) {
                    int id = scan.nextInt();
                    String name = scan.next();
                    String surname = scan.next();
                    String username = scan.next();
                    String password = scan.next();
                    System.out.println("Id: " + id + "\n" + "Name: " + name + "\n" + "Surname: " + surname + "\n" + "Username: " + username + "\n");
                }

                while (true) {

                    System.out.println("Select command:");
                    System.out.println("1. Write message");
                    System.out.println("2. My messages");
                    System.out.println("3. Back");
                    int ccc = sc.nextInt();
                    if (ccc == 1) {
                        System.out.println("Select id: ");
                        idd = sc.nextInt();
                        System.out.println("Write message: ");
                        messages = sc.next();
                        Message m1 = new Message(users.getGetId(), messages, idd);
                        message1.add(m1);
                        String content = "";
                        for (Message mes : message1) {
                            content += mes + "\n";
                            Files.write(Paths.get("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3message.txt"), content.getBytes());
                        }
                    } else if (ccc == 2) {
                        int a = 0;
                        while (aa.hasNext()) {
                            int first = aa.nextInt();
                            String message = aa.next();
                            int sec = aa.nextInt();
                            Message ms1 = new Message(first, message, sec);

                            if (users.getGetId() == sec) {
                                System.out.println("Message: " + message);
                                System.out.println("From id: " + first);
                                System.out.println("-----------------------");
                                a++;
                            }
                        }
                        if (a == 0) {
                            System.out.println("No messages!");
                        }
                    } else {
                        break;
                    }
                }
            } else if (choice == 2) {
                System.out.println("Select command:");
                System.out.println("1. Add friend");
                System.out.println("2. My friends");
                System.out.println("3. Back");
                int ccc = sc.nextInt();
                if (ccc == 1) {
                    System.out.println("Select id: ");
                    int idf = sc.nextInt();
                    Friend f1 = new Friend(users.getGetId(), idf);
                    friends.add(f1);
                    String content = "";
                    for (Friend free : friends) {
                        content += free + "\n";
                        Files.write(Paths.get("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3\\friend.txt"), content.getBytes());
                    }
                }
                if (ccc == 2) {
                    int a1 = 0;
                    while (bb.hasNext()) {
                        int firstfriend = bb.nextInt();
                        int secondfriend = bb.nextInt();
                        Friend free1 = new Friend(firstfriend, secondfriend);
                        if (users.getGetId() == firstfriend) {
                            System.out.println("----------------------------");
                            System.out.println("Your friend id: " + secondfriend + "\n");

                            a1++;
                        } else if (users.getGetId() == secondfriend) {
                            System.out.println("Your friend id: " + firstfriend + "\n");
                            a1++;
                        }
                        System.out.println("----------------------------");
                    }
                    if (a1 == 0) {
                        System.out.println("----------------------------");
                        System.out.println("You have not any friends!");
                        System.out.println("----------------------------");
                    }
                }
            } else if (choice == 3) {
                logOff();
                break;
            } else {
                break;
            }
        }
    }


    private void logOff() throws IOException {
        signedUser = null;
        menu();
    }

    private void authentication() throws IOException {

        while (true) {
            for (int i = 0; i < 10; i++) {
                System.out.println("\n");
            }
            System.out.println("Select command:");
            System.out.println("1. Sign up");
            System.out.println("2. Sign in");
            System.out.println("3. Back");
            int choice = sc.nextInt();
            if (choice == 1) {
                signUp();
                break;
            } else if (choice == 2) {
                signIn();
                break;
            } else {
                menu();
                break;
            }
        }
    }


    private void signIn() throws IOException {
        // File file = new File("C:\\data.txt");
        // Scanner fsc = new Scanner(file);

        System.out.println("Welcome to signUp");
        for (int i = 0; i < 10; i++) {
            System.out.println("\n") ;
        }
        System.out.println("Enter your username: ");
        String username;
        String password;
        int coun = 0;
        int c = 0;
        while (true) {
            username = sc.next();
            if (c == 0) {
                for (User user : users) {
                    coun++;
                    if (username.equals(user.getUsername())) {
                        c = 1;

                        break;
                    }
                }
                if (c != 1) {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("\n");
                    }
                    System.out.println("Incorrect username!");
                    System.out.println("Try again: ");
                }
            }
            if (c == 1) {
                break;
            }

        }

        for (int i = 0; i < 10; i++) {
            System.out.println("\n");
        }
        System.out.println("Enter password: ");


        int g = 0;
        while (true) {
            password = sc.next();
            if (g == 0) {
                for (User user : users) {
                    if (password.equals(user.getPassword())) {
                        g = 1;
                        break;
                    }
                }
                if (g != 1) {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("\n");
                    }
                    System.out.println("Incorrect password!");
                    System.out.println("Try again: ");
                }
            }
            if (g == 1) {
                break;
            }

        }
        for (int i = 0; i < 10; i++) {
            System.out.println("\n");
        }
        String name;
        String surname;
        int id = coun - 1;
        name = users.get(id).getName();
        surname = users.get(id).getSurname();
        User u2 = new User(id, name, surname, username, password);
        signedUser = users.get(id);
        userProfile(u2);


    }

    private void signUp() throws IOException {
        System.out.println("Welcome to signUp");

        System.out.println("Enter your name: ");
        String name = sc.next();

        for (int i = 0; i < 10; i++) {
            System.out.println("\n");
        }
        System.out.println("Enter your surname: ");
        String surname = sc.next();

        for (int i = 0; i < 10; i++) {
            System.out.println("\n");
        }
        String username;
        int c = 0;

        System.out.println("Enter your username: ");

        while (true) {

            username = sc.next();

            if (c == 0) {
                for (User user : users) {
                    if (username.equals(user.getUsername())) {
                        for (int i = 0; i < 10; i++) {
                            System.out.println("\n");
                        }
                        System.out.println("Enter another username: ");
                        c = 0;
                        break;
                    } else {
                        c++;
                    }
                }
            }
            if (c != 0) {
                break;
            }
        }

        for (int i = 0; i < 10; i++) {
            System.out.println("\n");
        }
        System.out.println("Enter your password: ");
        String password = sc.next();

        Password p = new Password(password);

        while (!p.checkPasswordFormat(password)) {
            for (int i = 0; i < 10; i++) {
                System.out.println("\n");
            }
            System.out.println("Error: Incorrect password format.");
            System.out.println("It should contain uppercase and lowercase letters and digits and its length must be more than 9 symbols!");
            System.out.println("Try again: ");
            password = sc.next();
            p.setPasswordStr(password);
        }

        User u3 = new User(name, surname, username, password);
        users.add(u3);
        String content = "";
        for (User user : users) {
            content += user + "\n";
            Files.write(Paths.get("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3\\data.txt"), content.getBytes());
        }
        authentication();

    }

    public void start() throws IOException {
        File file = new File("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3\\data.txt");
        Scanner fsa = new Scanner(file);
        File file1 = new File("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3\\message.txt");
        Scanner fsa1 = new Scanner(file1);
        File file2 = new File("C:\\Users\\user\\IdeaProjects\\assignment11\\src\\test\\java\\week3\\friend.txt");
        Scanner fsa2 = new Scanner(file2);
        // fill userlist from db.txt
        int counter = 0;

        while (fsa.hasNext()) {
            int id = fsa.nextInt();
            String name = fsa.next();
            String surname = fsa.next();
            String username = fsa.next();
            String password = fsa.next();
            if (id > counter) {
                counter = id;
            }
            User user = new User(id, name, surname, username, password);
            users.add(user);
        }
        users.get(0).setGenId(counter);

        while (fsa1.hasNext()) {
            int firstfriend = fsa1.nextInt();
            String message = fsa1.next();
            int secondfriend= fsa1.nextInt();
            Message mess = new Message(firstfriend, message, secondfriend);
            message1.add(mess);
        }

        while (fsa2.hasNext()) {
            int firstfriend = fsa2.nextInt();
            int secondfriend = fsa2.nextInt();
            Friend free = new Friend(firstfriend, secondfriend);
            friends.add(free);

        }


        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Close application");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
                break;
            } else {
                break;
            }
        }
    }
}
