package week3;


public class Password extends MyApplication {
    // passwordStr
    // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols

    private String passwordStr;


    public Password(String passwordStr) {
        setPasswordStr(passwordStr);
    }


    public String getPasswordStr() {
        return passwordStr;
    }

    public void setPasswordStr(String passwordStr) {
        if (checkPasswordFormat(passwordStr)) {
            this.passwordStr = passwordStr;
        }
    }

    public static boolean checkPasswordFormat(String password) {
        int c = 0;
        if (password.length() > 9) {
            for (int i = 0; i < password.length(); i++) {
                if (Character.isUpperCase(password.charAt(i))) {
                    c++;
                    break;
                }

            }
            for (int i = 0; i < password.length(); i++) {
                if (Character.isLowerCase(password.charAt(i))) {
                    c++;
                    break;
                }
            }
            for (int i = 0; i < password.length(); i++) {
                if (Character.isDigit(password.charAt(i))) {
                    c++;
                    break;
                }
            }
            if (c == 3) return true;
            else return false;

        } else {
            return false;
        }
    }
}