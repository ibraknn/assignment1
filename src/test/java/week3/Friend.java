package week3;

public class Friend extends MyApplication{
    private int firstfriend;
    private int secondfriend;

    public Friend(int firstfriend, int secondfriend){
        setFirstfriend(firstfriend);
        setSecondfriend(secondfriend);
    }

    public void setFirstfriend(int firstfriend){
        this.firstfriend=firstfriend;
    }

    public void setSecondfriend(int secondfriend){
        this.secondfriend=secondfriend;
    }
    public int getFirstfriend(){
        return firstfriend;
    }
    public int getSecondfriend(){
        return secondfriend;
    }

    @Override
    public String toString() {
        return firstfriend+" "+secondfriend;
    }
}
